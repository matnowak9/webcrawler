import org.jsoup.nodes.Document;

/**
 * Created by daisy on 02.05.16.
 */
public abstract class Filter {
    protected Filter nested;
    protected SearchParameter params;

    Filter(Filter nFilter, SearchParameter params){
        nested = nFilter;
        this.params = params;
    }

    abstract protected boolean checkCondition(Document doc);

    public final boolean check(Document doc){
        if (nested != null)
            return checkCondition(doc) && nested.check(doc);
        return checkCondition(doc);
    }
}
