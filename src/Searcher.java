import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by daisy on 02.05.16.
 */
public class Searcher {
    private String startLink;
    private Queue<String> linkQ;
    private Filter filter;

    Searcher(String startLink, Filter filter){
        this.filter = filter;
        this.startLink = startLink;
        linkQ = new LinkedList<>();
        linkQ.add(startLink);
    }

    public List<String> perform(){
        List<String> ret = new ArrayList<>();

        Map<String, Boolean> map = new HashMap<>();
        map.put(startLink, Boolean.TRUE);
        while (!linkQ.isEmpty()){
            String link = linkQ.poll();
            if (link.contains(".pdf")) continue;
            System.out.println("Sprawdzam "+link);
            Document doc = null;
            try {
                doc = Jsoup.connect(link).get();
            }catch(Exception e){
                e.printStackTrace();
                continue;
            }

            if(filter.check(doc)){
                ret.add(link);
                Elements links = doc.select("a[href]");
                for (Element e : links) {
                    String toAdd = e.attr("abs:href");
                    if (getDomainName(startLink).equals(getDomainName(toAdd))
                            && map.get(toAdd) == null){
                        map.put(toAdd, Boolean.TRUE);
                        linkQ.add(toAdd);
                        System.out.println("Dodaje "+toAdd);
                    }
                }
            }
        }

        return ret;
    }

    private String getDomainName(String url) {
        try{
            URI uri = null;
            uri = new URI(url);
            String domain = uri.getHost();
            if (domain != null)
                return domain.startsWith("www.") ? domain.substring(4) : domain;
        }catch(URISyntaxException e){
            e.printStackTrace();
        }
        return "";
    }
}
