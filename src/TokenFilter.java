import org.jsoup.nodes.Document;

/**
 * Created by daisy on 02.05.16.
 */
public class TokenFilter extends Filter {

    public TokenFilter(Filter nFilter, SearchParameter params){
        super(nFilter, params);
    }

    @Override
    protected boolean checkCondition(Document doc){
        return doc.body().text().contains(params.getArg(0));
    }
}
