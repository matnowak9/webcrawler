import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by daisy on 02.05.16.
 */
public class FilterFactory {
    private static FilterFactory factory = null;

    private FilterFactory(){
    }

    public static FilterFactory getFactory(){
        if (factory == null)
            factory = new FilterFactory();
        return factory;
    }

    public Filter createFilter(List<SearchParameter> list){
        Filter ret = null;

        for (int i=list.size()-1; i>=0; i--){
            try{
            ret = (Filter)Class.forName(list.get(i).getFilterClass().getName())
                    .getConstructor(Filter.class, SearchParameter.class)
                    .newInstance(ret, list.get(i));
            }catch (ClassNotFoundException e){
                e.printStackTrace();
            }catch(NoSuchMethodException e){
                e.printStackTrace();
            }catch(IllegalAccessException e){
                e.printStackTrace();
            }catch(InstantiationException e) {
                e.printStackTrace();
            }catch(InvocationTargetException e){
                e.printStackTrace();
            }

        }

        return ret;
    }
}
