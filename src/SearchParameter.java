/**
 * Created by daisy on 02.05.16.
 */
public class SearchParameter {
    private Class<? extends Filter> c;
    private String[] args;

    SearchParameter(Class<? extends Filter> c, String... args){
        this.c = c;
        this.args = args;
    }

    public String getArg(int n){
        return args[n];
    }

    public Class<? extends Filter> getFilterClass(){
        return c;
    }
}
