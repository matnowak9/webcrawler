import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daisy on 02.05.16.
 */
public class Controller {
    public static void main(String[] args) {
        String startLink = "http://www.tutorialspoint.com/uml/";

        List< SearchParameter > list = new ArrayList();
        list.add(new SearchParameter(TokenFilter.class, "UML"));
        list.add(new SearchParameter(TokenFilter.class, "Tutorial"));
        list.add(new SearchParameter(TokenFilter.class, "Diagram"));
        Filter f = FilterFactory.getFactory().createFilter(list);

        Searcher searcher = new Searcher(startLink, f);
        List<String> urls = searcher.perform();

        System.out.println("\nZnalezione strony:");
        for (String url : urls)
            System.out.println(url);
    }
}
